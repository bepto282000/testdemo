package com.example.test1.transformer.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentResponse implements Serializable {
    private Long id;
    private String fullName;
    private Long age;
    private String address;
}
