package com.example.test1.controller;

import com.example.test1.entity.Student;
import com.example.test1.repository.StudentRepositoty;
import com.example.test1.service.StudentService;
import com.example.test1.transformer.request.DeleteStudentRequest;
import com.example.test1.transformer.request.StudentRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class StudentController {


    private final StudentService studentService;

    // test demo git new brand

    private final StudentRepositoty repositoty;

    public StudentController(StudentService studentService, StudentRepositoty repositoty) {
        this.studentService = studentService;
        this.repositoty = repositoty;
    }


    // viết api thêm mới
    @PostMapping("/add")
    ResponseEntity<Student> createStudent(@RequestBody StudentRequest request) {
        return studentService.addStudent(request);
    }

    // api update data
    @PutMapping("/update")
    String updateStudent(@RequestParam Long age,
                         @RequestBody StudentRequest studentRequest) {
        return studentService.updateStudent(age, studentRequest);

    }

    // xóa
    @DeleteMapping
    String deletedStudent(@RequestBody DeleteStudentRequest request) {
        return studentService.deleteStudent(request);

    }


    @GetMapping("/detail/{id}")
    Optional<Student> getDetailStudent(@PathVariable Long id) {
        return studentService.getDetailStudent(id);
    }


    @PutMapping("/update/new")
    String updateNew(@RequestParam Long id, @RequestBody StudentRequest request) {
        return studentService.updateNew(id, request);
    }


    // API Get danh sách + Phân Trang + tìm kiếm
    @GetMapping("/students")
    ResponseEntity<Page<Student>> getStudentByPageAndSearch(@RequestParam(required = false) String fullName,
                                                            @RequestParam(required = false) Long age, Pageable pageable) {

        Page<Student> students = studentService.findStudentsByFullNameAndAge(fullName, age, pageable);
        return new ResponseEntity<>(students, HttpStatus.OK);


    }


}
