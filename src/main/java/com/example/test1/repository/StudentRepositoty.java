package com.example.test1.repository;

import com.example.test1.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface StudentRepositoty extends JpaRepository<Student,Long> {

    @Query("select s from Student s where s.age = ?1")
    Student findByAge(Long age);

   @Transactional
   @Modifying
   @Query("delete from Student s where s.id = ?1")
   void deleteById(Long id);



   @Query(value = "select * from Student ",nativeQuery = true)
    List<Student> findAllStudent();

    @Query("select s from Student s where s.fullName = ?1 and s.age = ?2")
    Page<Student> findByFullNameAndAge(@Param("fullName") String fullName,
                              @Param("age") Long age,
                              Pageable pageable);

    @Query("select s from Student s where upper(s.fullName) like upper(concat('%', ?1, '%'))")
    Page<Student> findByFullNameContainingIgnoreCase(String fullName, Pageable pageable);

    @Query("select s from Student s where s.age = ?1")
    Page<Student> findByAges(Long age, Pageable pageable);

    @Query("select s from Student s where upper(s.fullName) like upper(concat('%', ?1, '%')) and s.age = ?2")
    Page<Student> findByFullNameContainingIgnoreCaseAndAge(String fullName, Long age, Pageable pageable);
}
