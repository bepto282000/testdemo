package com.example.test1.service;

import com.example.test1.entity.Student;
import com.example.test1.repository.StudentRepositoty;
import com.example.test1.transformer.request.DeleteStudentRequest;
import com.example.test1.transformer.request.StudentRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private final StudentRepositoty repositoty;

    public StudentService(StudentRepositoty repositoty) {
        this.repositoty = repositoty;
    }

    public ResponseEntity<Student> addStudent(StudentRequest request) {
        Student student = new Student();
        student.setAge(request.getAge());
        student.setAddress(request.getAddress());
        student.setFullName(request.getFullName());

        repositoty.save(student);
        return ResponseEntity.ok(student);

    }

    public String updateStudent(Long age, StudentRequest studentRequest) {

        var student = repositoty.findByAge(age); // lấy ra đối tượng em muốn cập nhật theo id

        student.setAddress(studentRequest.getAddress());
        student.setFullName(studentRequest.getFullName());
        // cập nhật data

        repositoty.save(student); // lưu vào database

        return "Cập nhật oke xong"; // trả response ra màn UI UX
    }

    public String deleteStudent(DeleteStudentRequest request) {
        // điều kiện chỉ xóa id nào có age >10]
//        nếu cố tình xóa thì báo lỗi  n valid
        repositoty.deleteById(request.getId());
        return "Xóa thành công ok";
    }

    public List<Student> getStudentAll() {
        return repositoty.findAll(); // jpa hộ trợ
    }

    public Optional<Student> getDetailStudent(Long id) {
        return repositoty.findById(id);
    }

    public String updateNew(Long id, StudentRequest request) {
        var student = repositoty.findById(id).orElse(null);
        student.setFullName(request.getFullName());
        student.setAge(request.getAge());
        student.setAddress(request.getAddress());

        repositoty.save(student);
        return "Cập nhật thành công";
    }

    public Page<Student> findStudentsByFullNameAndAge(String fullName, Long age, Pageable pageable) {
        if (fullName == null && age == null) {
            return repositoty.findAll(pageable);
        }

        if (fullName != null && age == null) {
            return repositoty.findByFullNameContainingIgnoreCase(fullName, pageable);
        }

        if (fullName == null && age != null) {
            return repositoty.findByAges(age, pageable);
        }


        return repositoty.findByFullNameContainingIgnoreCaseAndAge(fullName, age, pageable);

    }
}
